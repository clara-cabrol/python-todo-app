FROM python
RUN pip install flask flask_cors
COPY app.py .
ENV FLASK_RUN_HOST=0.0.0.0
EXPOSE 5000
ENTRYPOINT [ "flask", "run" ]
