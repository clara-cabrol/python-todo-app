# Serveur en python utilisant la librairie FLASK = API, qui communique en JSON avec le navigateur mais codé en python
# FLASK_ENV=development flask run
from flask import Flask, jsonify, abort, request  # Flask = classe
from flask_cors import CORS
from uuid import uuid4

# On crée un objet Flask et Flask(__name__) représente le serveur
app = Flask(__name__)  # __name__ nom du fichier
CORS(app)

# Tableau = liste en python contenant des dictionnaires
tasks: list[dict] = [  # Typage
    {"statut": "todo", "description": "task1", "id": str(uuid4())},
    {"statut": "doing", "description": "task2", "id": str(uuid4())},
    {"statut": "done", "description": "task3", "id": str(uuid4())},
]

# /tasks GET, POST
@app.route(
    "/tasks", methods=["GET"]
)  # @ : décorateur permet de faire le lien entre le chemin /tasks et la méthode show_tasks
# La route ne répond que si c'est le GET
def show_tasks():
    return jsonify(tasks), 200  # Transforme l'objet python en JSON


@app.route("/tasks", methods=["POST"])
# La route ne répond que si c'est le POST
def add_task():
    if not request.is_json:  # si la requête client n'est pas du json
        # On renvoie une erreur client car la requête est mal formée
        # Le format média des données demandées n'est pas supporté par le serveur, donc le serveur rejette la requête.
        abort(415)

    json_body: dict = request.get_json()  # Si c'est du json, je récupère son contenu

    keys = set(json_body.keys())

    # Je vérifie que les clés statut et description existent
    if (len(keys.union({"statut", "description"})) is not 2) or len(
        json_body.keys()
    ) is 0:
        abort(
            400
        )  # Si le format de la tâche n'est pas le bon, on retourne un 400 Bad request

    json_body.update({"id": str(uuid4())})

    # Alors je peux ajouter la tâche à la liste tasks
    tasks.append(json_body)
    return "ok", 201


# /tasks/<int:id> GET, PUT, DELETE
@app.route("/tasks/<int:task_uuid>", methods=["GET", "PUT", "DELETE"])
# La route ne répond que si c'est le GET, ou PUT, ou DELETE
def show_task(task_uuid):
    # Suppression d'une tâche (DELETE)
    for index, task in enumerate(tasks):
        if task_uuid == task["id"]:
            found_task = task
            found_index = index

    if request.method == "DELETE":
        # pour enelever un élément de la liste, on utilise remove()
        tasks.remove(
            found_task
        )  # MAIS pour remove à partir d'un INDEX on utilise pop()
        return "ok", 204

    # Mise à jour d'une tâche (PUT)
    if request.method == "PUT":
        tasks[found_index] = request.get_json()  # renvoie un dict python
        return tasks[found_index], 202

    return tasks[found_index], 200
